<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'wpdemo');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', '');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', '[>NV ;C<:nUS(01K9T+v5c,%:m6+45,hZYM!9-@H*YB|j%-Q6dYo4,`rFo/nCRyn');
define('SECURE_AUTH_KEY', 'm4063`ghy3|9y&Q$;ooLwL!++!vQF5)tTjJ0%k1rw$)@F4l6?]1]vhc`HAZo<ed0');
define('LOGGED_IN_KEY', ')</|(`e8V:A.Vnfn.$!#8rO!ex82K^0Ui+mAD<r+fe}7K*8^RHN[ZMk>{s6EtWP-');
define('NONCE_KEY', '+Y~LLK,T`FEJaX2,|<+{Yb&<O-}/T=Q3G{20FHZKo%uqwf+%)a|3F};yiV7f~3u)');
define('AUTH_SALT', ']DbEL3*;!q>} SO=ar8G=cjK_4r2_B83dbu|)qo!{rro -5_V;.S98}qXIbF@#N4');
define('SECURE_AUTH_SALT', 'iY#l{>%g$VOHAZo`>rQYm;B)3%0HmJ+S~x.UIyO-T7X+V,[lZs|d+c2qR;^1DZjt');
define('LOGGED_IN_SALT', ')-Pzyjdt{mWM:Q%=$B.4VQQ6(w4Sk O^b_-|ZyDiJJq+.,R77)LT8vK5B:ZMO-rs');
define('NONCE_SALT', '=`@//:Nr!yvSb!3UT9z|?C~R%l/[`+<1yRe7L6;NEI A,&r9&=-@W]*XLP8>j}Fp');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wpdemo_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

